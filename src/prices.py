from src.html import Event, State, stream_html_events

from _collections_abc import Generator
from typing import Generator, NamedTuple


class HistoricalPrice(NamedTuple):
    date: str
    open: str
    high: str
    low: str
    close: str
    adj_close: str
    volume: str


def extract_from_html(
    data: Generator[str, None, None]
) -> Generator[HistoricalPrice, None, None]:
    events = stream_html_events(data)
    filtered_events = historical_price_events(events)
    prices = historical_prices(filtered_events)
    return prices
    

def historical_price_events(
    events: Generator[Event, None, None]
) -> Generator[Event, None, None]:
    table_tag = "tbody"
    in_table = False

    for event in events:
        if event.state == State.TAG_START:
            tag = event.value
            if tag == table_tag:
                in_table = True
                continue

        if event.state == State.TAG_END:
            tag = event.value
            if tag == table_tag:
                in_table = False
                return
        
        if in_table:
            if event.state == State.DATA:
                yield event


def historical_prices(
    events: Generator[Event, None, None]
) -> Generator[HistoricalPrice, None, None]:
    collection = []
    size = 7
    
    for idx, event in enumerate(events, start=1):
        collection.append(event.value)
        if idx % size == 0:
            yield HistoricalPrice(*collection)
            collection = []
