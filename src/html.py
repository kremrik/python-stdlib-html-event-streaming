from collections import deque
from _collections_abc import Generator
from enum import Enum, auto
from html.parser import HTMLParser
from typing import Generator, NamedTuple


class State(Enum):
    TAG_START = auto()
    TAG_END = auto()
    DATA = auto()


class Event(NamedTuple):
    state: State
    value: str


def stream_html_events(
    text: Generator[str, None, None]
) -> Generator[Event, None, None]:
    events = _get_events(text)
    coalesced = _coalesce_data_events(events)
    yield from coalesced


def _get_events(
    text: Generator[str, None, None]
) -> Generator[Event, None, None]:
    parser = _HTMLEventStream()
    for chunk in text:
        yield from parser.feed(chunk)


def _coalesce_data_events(
    events: Generator[Event, None, None]
) -> Generator[Event, None, None]:
    for event in events:
        if event.state == State.DATA:
            data = ""
            while event.state == State.DATA:
                data += event.value
                event = next(events)
            yield Event(state=State.DATA, value=data)
        yield event


class _HTMLEventStream(HTMLParser, Generator):
    def __init__(self):
        HTMLParser.__init__(self)
        self.parsed = deque()

    def send(self, _):
        try:
            return self.parsed.popleft()
        except IndexError:
            self.throw()

    def throw(self, type=None, value=None, traceback=None):
        raise StopIteration
    
    def handle_starttag(self, tag, attrs):
        # we don't even need to look at CSS selectors
        del attrs
        self.parsed.append(
            Event(state=State.TAG_START, value=tag)
        )

    def handle_endtag(self, tag):
        self.parsed.append(
            Event(state=State.TAG_END, value=tag)
        )

    def handle_data(self, data):
        if data.strip():
            self.parsed.append(
                Event(state=State.DATA, value=data)
            )

    def feed(self, data: str):
        super().feed(data)
        return self
