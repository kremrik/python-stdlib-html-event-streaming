from src import prices as ehp

import unittest
from textwrap import dedent


class TestExtractHistoricalPrices(unittest.TestCase):
    def test_degenerate_case(self):
        html = "<html></html>"
        data = (line for line in html.splitlines())
        expect = []
        actual = list(ehp.extract_from_html(data))
        self.assertEqual(expect, actual)

    def test_one_price(self):
        html = dedent("""
            <html>
                <tbody>
                    <tr class="BdT Bdc($seperatorColor) Ta(end) Fz(s) Whs(nw)">
                        <td class="Py(10px) Ta(start) Pend(10px)"><span>Aug 08, 2023</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,498.03</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,503.31</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,464.39</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>3,884,910,000</span></td>
                    </tr>
                </tbody>
            </html>
        """)
        data = (line for line in html.splitlines())
        expect = [
            ehp.HistoricalPrice(
                date="Aug 08, 2023",
                open="4,498.03",
                high="4,503.31",
                low="4,464.39",
                close="4,499.38",
                adj_close="4,499.38",
                volume="3,884,910,000",
            )
        ]
        actual = list(ehp.extract_from_html(data))
        self.assertEqual(expect, actual)

    def test_mult_prices(self):
        html = dedent("""
            <html>
                <tbody>
                    <tr class="BdT Bdc($seperatorColor) Ta(end) Fz(s) Whs(nw)">
                        <td class="Py(10px) Ta(start) Pend(10px)"><span>Aug 08, 2023</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,498.03</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,503.31</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,464.39</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>3,884,910,000</span></td>
                    </tr>
                    <tr class="BdT Bdc($seperatorColor) Ta(end) Fz(s) Whs(nw)">
                        <td class="Py(10px) Ta(start) Pend(10px)"><span>Aug 08, 2023</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,498.03</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,503.31</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,464.39</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>3,884,910,000</span></td>
                    </tr>
                </tbody>
            </html>
        """)
        data = (line for line in html.splitlines())
        expect = [
            ehp.HistoricalPrice(
                date="Aug 08, 2023",
                open="4,498.03",
                high="4,503.31",
                low="4,464.39",
                close="4,499.38",
                adj_close="4,499.38",
                volume="3,884,910,000",
            ),
            ehp.HistoricalPrice(
                date="Aug 08, 2023",
                open="4,498.03",
                high="4,503.31",
                low="4,464.39",
                close="4,499.38",
                adj_close="4,499.38",
                volume="3,884,910,000",
            ),
        ]
        actual = list(ehp.extract_from_html(data))
        self.assertEqual(expect, actual)

    def test_only_fetch_first_table(self):
        html = dedent("""
            <html>
                <tbody>
                    <tr class="BdT Bdc($seperatorColor) Ta(end) Fz(s) Whs(nw)">
                        <td class="Py(10px) Ta(start) Pend(10px)"><span>Aug 08, 2023</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,498.03</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,503.31</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,464.39</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>3,884,910,000</span></td>
                    </tr>
                    <tr class="BdT Bdc($seperatorColor) Ta(end) Fz(s) Whs(nw)">
                        <td class="Py(10px) Ta(start) Pend(10px)"><span>Aug 08, 2023</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,498.03</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,503.31</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,464.39</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>3,884,910,000</span></td>
                    </tr>
                </tbody>
                <tbody>
                    <tr class="BdT Bdc($seperatorColor) Ta(end) Fz(s) Whs(nw)">
                        <td class="Py(10px) Ta(start) Pend(10px)"><span>Aug 08, 2023</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,498.03</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,503.31</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,464.39</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>3,884,910,000</span></td>
                    </tr>
                </tbody>
            </html>
        """)
        data = (line for line in html.splitlines())
        expect = [
            ehp.HistoricalPrice(
                date="Aug 08, 2023",
                open="4,498.03",
                high="4,503.31",
                low="4,464.39",
                close="4,499.38",
                adj_close="4,499.38",
                volume="3,884,910,000",
            ),
            ehp.HistoricalPrice(
                date="Aug 08, 2023",
                open="4,498.03",
                high="4,503.31",
                low="4,464.39",
                close="4,499.38",
                adj_close="4,499.38",
                volume="3,884,910,000",
            ),
        ]
        actual = list(ehp.extract_from_html(data))
        self.assertEqual(expect, actual)

    def test_newline_mid_data(self):
        html = dedent("""
            <html>
                <tbody>
                    <tr class="BdT Bdc($seperatorColor) Ta(end) Fz(s) Whs(nw)">
                        <td class="Py(10px) Ta(start) Pend(10px)"><span>Aug 08, \n2023</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,498.03</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,503.31</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,464\n.39</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>4,499.38</span></td>
                        <td class="Py(10px) Pstart(10px)"><span>3,88\n4,910,000</span></td>
                    </tr>
                </tbody>
            </html>
        """)
        data = (line for line in html.splitlines())
        expect = [
            ehp.HistoricalPrice(
                date="Aug 08, 2023",
                open="4,498.03",
                high="4,503.31",
                low="4,464.39",
                close="4,499.38",
                adj_close="4,499.38",
                volume="3,884,910,000",
            )
        ]
        actual = list(ehp.extract_from_html(data))
        self.assertEqual(expect, actual)
