# S&P500 historical prices HTML extraction

## Overview
I couldn't download data on the S&P500 as far back as I wanted from [Yahoo Finance](https://finance.yahoo.com/quote/%5EGSPC/history?period1=-1325635200&period2=1691625600&interval=1d&filter=history&frequency=1d&includeAdjustedClose=true), but I _could_ use the infinite scroll feature to at least see those prices. And from there it was possible to just copy out the HTML from the developer tools. That just left the need for a quick-and-dirty script to extract the desired data and prepare it for conversion to a CSV.

## Goals
1. Extract the historical table data from Yahoo Finance HTML data
1. No 3rd party libraries. If Python didn't have one, I would have found a different language (maybe Racket or Nim)
1. Take less than a half day
1. Have fun (this stuff keeps you sharp!)

## Non-goals
1. Make this thing "generalizable" for other market sites, other HTML page layouts, other data formats (CSV desired, everything can be a string), etc

## Interesting callouts
1. This is a clear-cut data transformation problem. As such, I used one of my favorite patterns: generator transformation pipelines. Lazy sequences can cause a lot of headaches (annoying edge-case logic, side effects, early upstream failures, etc), but for highly consistent and structured data like this there's generally less to worry about. Write tests regardless.
1. I converted the standard library `html.parser.HTMLParser` into a Generator using a `_collections_abc.Generator` mixin, which allowed me to isolate the parsing behavior to the very edge of the system rather than letting its API dictate the structure/statefulness/etc. It's also what enables the pipeline-style architecture.

## Requirements
There are no requirements other than a reasonably up-to-date Python on your PATH.

## Running tests
`python -m unittest discover -v`

## Data
There is a dump of data under the `./data` directory that can be used.
Since the `extract_from_html` function takes a string generator, you can buffer the input with something like
```python
def buffered_reader(path, buff_size=1024*10):
    with open(path, "r") as f:
        while True:
            data = f.read(buff_size)
            if not data:
                return
            yield data
```

Then you can just wire it up like
```python
from extract_historical_prices import extract_from_html

chunks = buffered_reader("./data/s&p500-historical-daily.html")
prices = extract_from_html(chunks)
for idx, price in enumerate(prices):
    if idx > 10:
        # don't print out _everything_ rn...
        break
    print(price)
```
